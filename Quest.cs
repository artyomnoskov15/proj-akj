Console.WriteLine("Игра разворачивается в древнем средневековом королевстве Лансерион, " +
    "наполненном мистическими существами и магическими артефактами. " +
    "Мало кто слышал про это место, потому что оно скрыто от посторонних глаз магическим барьером, " +
    "не пропускающим обычных смертных");
Console.WriteLine("\n"+"Нажмите Enter для продолжения");
Console.ReadLine();
Console.Clear();

Console.WriteLine("Вы рано осиротевший юноша, потерявший своих родителей из-за Катаклизма. " +
    "Вас спас один из королевских рыцарей во время пожара в приютском доме, " +
    "после чего вы решил нести добро в этот мир и стать герцогом королевства Лансерион");
Console.WriteLine("\n" + "Нажмите Enter для продолжения");
Console.ReadLine();
Console.Clear();

Console.WriteLine("\n" + "Введите имя своего персонажа");
Player player=new Player(Console.ReadLine());
Console.Clear();

Console.WriteLine("Для повышения ранга нужно достигнуть 25 уровня.\n");
    //"\nВы можете пойти взять задание в гильдии или же пойти в подземелье"+"\nВведите 1 или 2 в соответсвии");
Random rnd=new Random();
int x = 0; // переменная созданная для хранения значений получающихся при rnd.Next
int lvl_of_depth = 0; // глубина погружение в подземелье
int old_player_lvl = 0; // старый уровень игрока, нужен для подсчёта повышений
int number=0; // номер квеста который попадётчя игроку
int k=0; // счётчик количества просмотренных квестов
List<string> quests = new List<string>() {"Зачистить подземелье","Спасти мирного жителя","Сразится на арене"
    ,"Очистить захваченную деревню","Поучаствовать в лотерее"}; // список названий квестов
List<string> names_of_citizens = new List<string>() {"Карл","Джон","Макс","Сэм","Серафим","Александер" }; // список имён жителей для спасения
bool works=false; // проверка на неправильно введённое значение при выборе квеста
while (player.Level<65) // условие окончания игры победой
{
    works=false;
    if ((player.Level>=25 && old_player_lvl<25) || (player.Level >= 35 && old_player_lvl < 35) 
        || (player.Level >= 45 && old_player_lvl < 45) || (player.Level >= 60 && old_player_lvl < 60))
    {
        player.LevelUps(); // вывод в консоль повышений уровня игрока
    }
    while (!works) // Цикл для отловки ошибок
    {
        try
        {
            Console.WriteLine("Вы можете пойти взять задание в гильдии или же пойти в подземелье\n1 - [ЗАДАНИЕ]\n2 - [ПОДЗЕМЕЛЬЕ]");
            x = Convert.ToInt32(Console.ReadLine()); // игрок выбирает действие
            switch (x) 
            {
                case 1: 
                    Console.WriteLine(Questhub()); 
                    works = true; 
                    Console.WriteLine("Взять квест?\n1 - [ДА]\n2 - [НЕТ]");
                    x= Convert.ToInt32(Console.ReadLine());
                    Console.Clear();
                    if(x==1)
                    Play_Quest(number+1); //вызывается метод в котором происходит квест
                    else
                    {
                        while (k<2) // возможность выбрать другой квест ограниченное количество раз
                        {
                            Console.WriteLine(Questhub());
                            Console.WriteLine("Взять квест?\n1 - [ДА]\n2 - [НЕТ]");
                            x = Convert.ToInt32(Console.ReadLine());
                            Console.Clear();
                            if (x == 1) k = 2;
                            k++;
                        }
                        Console.WriteLine("Вы берёте последний квест");
                        Play_Quest(number + 1);
                    }
                    k = 0;
                    break;
                case 2:
                    Console.WriteLine($"Вы вошли в подземелье, ваш текущий уровень погружения - {lvl_of_depth}");
                    Dungeon(); // вызывает метод поземелья
                    lvl_of_depth++; // увеличивает глубину погружения
                    Console.WriteLine("Ваши характеристики:");
                    player.ShowStats();// метод выводящий в консоль характеристики
                    works = true; break;
                default: throw new Exception("asd") ;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine("Введено некорректное значение, введите заново");
        }
    }
}
void Play_Quest(int number) // метод с воспроизведением выбранного квеста
{
    //number = 1;
    Enemy en = null;
    switch (number) //переключатель на определённый квест
    {
        case 1:
            int number_of_enemies =rnd.Next(1,5);
            //Enemy en;
            Console.WriteLine($"Вы зашли в подземелье, для победы вам нужно победить {number_of_enemies} врагов");
            List<Enemy> enemies= new List<Enemy>();//список противников в подземелье
            for (int i=0;i<number_of_enemies;i++)
            {
                x = rnd.Next(1,5);
                switch (x) // добавляет случайных противников в список
                {
                    case 1: enemies.Add(new Skeleton(player.Level)); break;
                    case 2: enemies.Add(new Goblin(player.Level)); break;
                    case 3: enemies.Add(new Ogre(player.Level)); break;
                    case 4: enemies.Add(new Troll(player.Level)); break;
                }
            }
            for (int i = 0; i < number_of_enemies; i++) // вызывает метод драки для каждого противника
            {
                Battle(player, enemies[i]); 
            }
            old_player_lvl = player.Level;
            player.Level = 3;
            Console.WriteLine("Поздравляем вы успешно зачистили подземелье! Ваши характеристики:");
            player.ShowStats();
            break;
            
        case 2:
            x=rnd.Next(1,5);
            switch (x)
            {
                case 1: en = new Skeleton(player.Level); break;
                case 2: en = new Goblin(player.Level); break;
                case 3: en = new Ogre(player.Level); break;
                case 4: en = new Troll(player.Level); break;
            }
            Console.WriteLine(names_of_citizens[rnd.Next(names_of_citizens.Count)]+" попал в беду, его окружили 3 "+en.Name); 
            for (int i = 0; i < 3; i++)
            {
                Battle(player, en);
            }
            old_player_lvl = player.Level;
            player.Level = 2;
            player.backpack[rnd.Next(0,3)]+=1; // добавляет зелье в рюкзак
            Console.WriteLine("Вы спасли жителя , в награду житель дал вам зелье: ");
            player.ShowBackpack();
            break;
        case 3:
            Console.WriteLine("Вы сражаетесь на арене против Огра");
            en=new Ogre(player.Level);
            Battle(player, en);
            Console.WriteLine("Поздравляем вы успешно зачистили подземелье! Ваши характеристики:");
            old_player_lvl = player.Level;
            player.Level = 2;
            player.ShowStats();
            break;
        case 4: 
            number_of_enemies = rnd.Next(3,7);
            Console.WriteLine($"Вы собираетесь очистить одну из захваченных деревень, количество противников - {number_of_enemies}.\n");
            enemies = new List<Enemy>();
            for (int i = 0; i < number_of_enemies; i++)
            {
                x = rnd.Next(1, 5);
                switch (x)
                {
                    case 1: enemies.Add(new Skeleton(player.Level)); break;
                    case 2: enemies.Add(new Goblin(player.Level)); break;
                    case 3: enemies.Add(new Ogre(player.Level)); break;
                    case 4: enemies.Add(new Troll(player.Level)); break;
                }
            }
            for (int i = 0; i < number_of_enemies; i++)
            {
                Battle(player, enemies[i]);
            }
            old_player_lvl = player.Level;
            player.Level = 5;
            Console.WriteLine("Поздравляем вы успешно освободили деревню! Ваши характеристики:");
            player.ShowStats();
            break;
        case 5: 
            Console.WriteLine("Вы участвуете в лотерее, бросить кубик?\n1 - [ДА]\n2 - [НЕТ]");
            x=Convert.ToInt32(Console.ReadLine());
            Console.Clear();
            if (x==1)
            {
                if (rnd.Next(1, 50) > 45)
                {
                    Console.WriteLine("Вы выиграли! Ваш уровень увеличен на 2");
                    player.Damage += 2;
                }
                else
                {
                    Console.WriteLine("Вы проиграли, ваше здоровье уменьшено на 10");
                    player.HP -= 10;
                }
            }
            break;
        default: throw new Exception();
    }
}
void Battle(Player player, Enemy enemy) // метод дуэли
{
    Console.WriteLine($"\nПервый ход за вами, здоровье противника - {enemy.Health}");
    while (enemy.Health>0) // проверка на то жив ли противник
    {
        Console.WriteLine("Что будете делать:\n1) Ударить соперника\n2) Открыть инвентарь\n3) Защититься\n4) Попробовать сбежать");
        x = Convert.ToInt32(Console.ReadLine()); // Выбор действия
        Console.Clear();
        int defstat = 0;// переменная для временного увеличения защиты при действии защищаться
        switch (x) // совершение выбранного действия
        {
            case 1:
                enemy.Health -= player.Damage;
                Console.WriteLine($"Вы успешно нанесли удар противнику, его здоровье теперь - {enemy.Health}");
                if (enemy.Health>=0)
                {
                    player.HP -= enemy.Damage - player.Defense;
                    Console.WriteLine($"Ход противника - противник бьёт на {enemy.Damage} урона, ваше здоровье теперь {player.HP}");
                }
                else { Console.WriteLine("Вы убили противника.\n"); }
                break;
                break;
            case 2: Console.WriteLine($"Вам доступно:\nИсцеляющих зелий - {player.backpack[0]}\nЗелий урона - {player.backpack[1]}\nЗелий защиты - {player.backpack[2]}");
                if (player.backpack[0]==0 && player.backpack[1]==0 && player.backpack[0]==0)
                {
                    Console.WriteLine("У вас ничего нет.");
                    break;
                }
                x=Convert.ToInt32(Console.ReadLine());
                switch (x)
                {
                    case 1: player.HP += rnd.Next(10,41);break;
                    case 2: player.Damage += rnd.Next(1, 7);break;
                    case 3: player.Defense += rnd.Next(3, 13); break;
                }
                player.HP -= enemy.Damage - player.Defense;
                Console.WriteLine($"Ход противника - противник бьёт на {enemy.Damage} урона, ваше здоровье теперь {player.HP}");
                break;
            case 3: defstat = rnd.Next(7, 12);
                Console.WriteLine($"Ваша защита укрепилась на {defstat}");
                player.Defense += defstat;
                if (player.Defense < enemy.Damage)
                {
                    player.HP -= enemy.Damage - player.Defense;
                }
                player.Defense-= defstat;
                Console.WriteLine($"Ход противника - противник бьёт на {enemy.Damage} урона, ваше здоровье теперь {player.HP}");
                break;
            case 4:
                //x = rnd.Next(1,11);
                if (rnd.Next(1, 11)>8)
                {
                    Console.WriteLine("Вам повезло и вы сбежали");
                    enemy.Health = 0;
                }
                else
                {
                    Console.WriteLine("У вас не получилось сбежать");
                    player.HP -= enemy.Damage - player.Defense;
                    Console.WriteLine($"Ход противника - противник бьёт на {enemy.Damage} урона, ваше здоровье теперь {player.HP}");
                    break;
                }
                break;
        }
    }
}
void Dungeon() // метод подземелья 
{
    Enemy enemy = null;
    x = rnd.Next(1, 5);
    switch (x)
    {
        case 1: enemy = new Skeleton(lvl_of_depth+2); break;
        case 2: enemy = new Goblin(lvl_of_depth + 2); break;
        case 3: enemy = new Ogre(lvl_of_depth + 2); break;
        case 4: enemy = new Troll(lvl_of_depth + 2); break;
    }
    Console.WriteLine($"Ваш противник - {enemy.Name}\nУр - {enemy.Level}");
    Battle(player, enemy);
    player.Level = 1;

}
string Questhub() // метод для хранения данных о последнем выданном квесте
{
    string quest ="";
    number=rnd.Next(quests.Count);
    quest = quests[number];
    return quest;
}
