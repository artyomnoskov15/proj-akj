namespace Quest
{
    internal class Player
    {
        Random rnd = new Random();
        int level, def, health, damage; // поля для хранения показателей игрока
        public int[] backpack = new int[] { 0, 0, 0 }; // Порядок зелий - HP, Damage, Defense

        public Player(string name)
        { 
            this.Level = 1; 
            this.Name = name; 
            health = 100; 
            damage = rnd.Next(5, 11); 
            def = rnd.Next(4, 11); 
        }
        public void ShowStats() // метод выводящий на экран статы игрока
        {
            Console.WriteLine("\nВаш уровень-"+Level+"\nВаше здоровье-"+HP+"\nВаш урон-"+Damage+"\nВаша защита-"+Defense);
        }
        public void LevelUps() // метод с проверкой на повышение
        {
            if (Level >= 60)
            {
                Console.WriteLine("\nВы повысили уровень и стали герцогом");
                Console.WriteLine("Вы победили. Нажмите любую кнопку чтобы выйти");
                Console.ReadLine();
                Environment.Exit(0);
            }
            else if (Level >= 45) Console.WriteLine("\nВы повысили уровень и стали графом");
            else if (Level >= 35) Console.WriteLine("\nВы повысили уровень и стали бароном");
            else if (Level >= 25) Console.WriteLine("\nВы повысили уровень и стали рыцарем");
        }
        public void ShowBackpack() => Console.WriteLine("\nЗелий здоровья - " + backpack[0]+"\nЗелий урона - " + backpack[1]+
            "\nЗелий защиты - " + backpack[2]); // метод выводящий количество зелий в рюкзаке
        public string Name { get; private set; }
        public int HP { get { return health; } set
            {
                health = value;
                if (health <= 0) //проверка на смерть игрока после изменения здоровья
                {
                    Console.WriteLine("Вы погибли. Нажмите любую кнопку чтобы выйти");
                    Console.ReadLine();
                    Environment.Exit(0); // завершение игры при смерти
                }
            }
        }
        public int Damage { get { return damage; } set { damage = value; } }
        public int Defense { get { return def; } set { def = value; } }
        public int Level { get { return level; } 
            set { level += value; 
                damage += 10 * value; 
                def += 10 * value; 
                HP += 50; 
                if (HP > 100) HP = 100; } } // изменение характеристик при повышении уровня
    }
}
