namespace Quest
{
    internal abstract class Enemy
    {
        protected string name;
        protected int lvl;
        protected int health;
        protected int damage;

        public Enemy(int lvl)
        {
        }
        public int Health
        {
            get { return health; }
            set { health= value; }
        }
        public int Damage => damage;
        public string Name => name;
    }
}
