namespace Quest
{
    internal class Skeleton : Enemy
    {
        Random rnd = new Random();
        public Skeleton(int lvl) : base(lvl)
        {
            name = "Skeleton";
            this.damage = rnd.Next(4, 8) + (lvl * 10);
            this.health = rnd.Next(7, 11) + (lvl * 10);
        }
    }
}
