namespace Quest
{
    internal class Goblin : Enemy
    {
        Random rnd = new Random();
        public Goblin(int lvl) : base(lvl)
        {
            name = "Goblin";
            this.damage = rnd.Next(3, 6) + (lvl * 10);
            this.health = rnd.Next(2, 5) + (lvl * 10);
        }
    }
}
