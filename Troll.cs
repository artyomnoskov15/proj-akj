namespace Quest
{
    internal class Troll : Enemy
    {
        Random rnd = new Random();
        public Troll(int lvl) : base(lvl)
        {
            name = "Troll";
            this.damage = rnd.Next(7, 11) + (lvl * 10);
            this.health = rnd.Next(5, 9) + (lvl * 10);
        }
    }
}
